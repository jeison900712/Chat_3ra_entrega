package com.example.pinamazonia;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class GrupoContactosActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.grupo_contactos);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.grupo_contactos, menu);
		return true;
	}

}
