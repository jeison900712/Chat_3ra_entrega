﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinAmazonia
{
   public class Estado
    {
        private String esta;

        public String Esta
        {
            get { return esta; }
            set { esta = value; }
        }

        public Estado() { }

        public Estado(string esta) {
            Esta = esta;
        }
    }
}
