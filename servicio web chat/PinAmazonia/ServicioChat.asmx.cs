﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PinAmazonia
{
    /// <summary>
    /// Descripción breve de ServicioChat
    /// </summary>
    [WebService(Namespace = "http://demo.chat.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class ServicioChat : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }

        [WebMethod]
        public Boolean insertar_mensaje(String remi, String desti, String mensa)
        {
            data_chatDataContext data = new data_chatDataContext();
            data.insertar_mensaje(remi, desti, mensa);

            return true;
        }


        [WebMethod]
        public Boolean insertar_Estado(String estado)
        {
            data_chatDataContext data = new data_chatDataContext();
            data.insertar_estado(estado);

            return true;
        }

        [WebMethod]
        public string insertar_usuario(String usuario)
        {
            data_chatDataContext data = new data_chatDataContext();
            data.insertar_usuario(usuario);

            return usuario;
        }


        [WebMethod]
        public List<Mensaje> getmensaje(String remitente, String mensaje)
        {
            List<Mensaje> lista = new List<Mensaje>();

            data_chatDataContext data = new data_chatDataContext();

            var resultado = data.listar_mensaje(remitente, mensaje);

            foreach (var usuario in resultado)
            {

                lista.Add(new Mensaje(
                   usuario.fk_usuario_remi,
                    usuario.mensaje

                   ));

            }
            return lista;
        }

        [WebMethod]
        public List<Estado> getestado(String estado)
        {
            List<Estado> lista = new List<Estado>();

            data_chatDataContext data = new data_chatDataContext();

            var resultado = data.listar_estado(estado);

            foreach (var usuario in resultado)
            {

                lista.Add(new Estado(
                   usuario.nom_estado


                   ));

            }
            return lista;
        }

        [WebMethod]
        public List<Perfil> getperfil(String perfil)
        {
            List<Perfil> lista = new List<Perfil>();

            data_chatDataContext data = new data_chatDataContext();

            var resultado = data.listar_perfil(perfil);

            foreach (var usuario in resultado)
            {

                lista.Add(new Perfil(
                   usuario.nombre,
                   usuario.nom_estado


                   ));

            }
            return lista;
        }

        [WebMethod]
        public List<Chat> getchat(String perfil)
        {
            List<Chat> lista = new List<Chat>();

            data_chatDataContext data = new data_chatDataContext();

            var resultado = data.listar_chat(perfil);

            foreach (var usuario in resultado)
            {

                lista.Add(new Chat(
                   usuario.fk_usuario_desti



                   ));

            }
            return lista;
        }
    }
}
